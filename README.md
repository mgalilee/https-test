# Description

Simplest CBNG JNLP project, that reads from an https resource (`https://sources.cern.ch/browse/~raw,r=HEAD/FESA-rep/branches/fesa-class/DQAmx/LS2/src/DQAmx/Common/calibre.tsv`).

# How to use

Run `bob assemble` from the project directory.

The test with different `jws`:
* `/mcr/bin/jws8 --wait-exit build/jnlp/https-test-0.0.1-https-test.jnlp`
* `/mcr/bin/jws11 --wait-exit build/jnlp/https-test-0.0.1-https-test.jnlp`
* `/mcr/bin/jws --wait-exit build/jnlp/https-test-0.0.1-https-test.jnlp`

Correct output ends with:

```
...
INFO: I_MAG	-1.5258789E-5	32768
INFO: CFG_CNT	1.0	0.0	
INFO: DQQDU_CFG_VAL	1.0	0.0	
INFO: DQAMG_CFG_VAL	1.0	0.0	
INFO: PARAM_VAL	1.0	0.0	
```

Incorrect output ends with:

```
...
ERROR: Caused by: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
ERROR: 	at java.base/sun.security.provider.certpath.SunCertPathBuilder.build(SunCertPathBuilder.java:141)
ERROR: 	at java.base/sun.security.provider.certpath.SunCertPathBuilder.engineBuild(SunCertPathBuilder.java:126)
ERROR: 	at java.base/java.security.cert.CertPathBuilder.build(CertPathBuilder.java:297)
ERROR: 	at java.base/sun.security.validator.PKIXValidator.doBuild(PKIXValidator.java:380)
ERROR: 	... 27 more
```