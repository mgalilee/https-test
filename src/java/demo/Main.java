/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package demo;

import java.io.InputStream;
import java.net.URL;

public class Main {

    public static void main(String[] args) throws Exception {
        URL url = new URL("https://sources.cern.ch/browse/~raw,r=HEAD/FESA-rep/branches/fesa-class/DQAmx/LS2/src/DQAmx/Common/calibre.tsv");
        InputStream input = url.openStream();
        byte[] buf = new byte[8192];
        int length;
        while ((length = input.read(buf)) > 0) {
            System.out.write(buf, 0, length);
        }
    }

}
